# parameters file
import numpy as np

from enum import Enum

E = 12                                                                  # the energy of the x-rays
LAMBDA =  1.239e-9 / E                                                  # the wavelength of the x-rays
f_d_dist = 1.2                                                          # the distance between the sample and the detector
pixel_size = 2*55e-6                                                    # the pixel size of the detector
two_theta        = 15.6*2*pi/180                                        # the twice Bragg angle
alpha_i                = 15.6*pi/180                                    # the incident angle
alpha_f                = two_theta - alpha_i                            # the scattered angle
delta_omega            = 0.016*np.pi/180
H, W                   = 200, 180                                       # size of the detector
step_num               = 27                                             # the number of angular steps in the measurements
num_scan_v             = 45                                             # number of scan positions along py
num_scan_h             = 41                                             # number of scan positions along px
step_scan_v            = 50e-9                                          # scan step size along py
step_scan_h            = 50e-9                                          # scan step size along px
scan_neg_v             = 1                                              # 1 if py is opposite of q2 (or r2), 0 otehrwise
scan_neg_h             = 1                                              # 1 if px is opposite of q_RC, 0 otehrwise
swap_fast_slow         = 1                                              # 1 if fast and slow axes are swapped
object_thickness       = 190*1e-9                                       # for object support


class PARAMETERS_NAMES(Enum):
    E = 'energy', # the energy of the x-rays
    LAMBDA = 'lambda', # the wavelength of the x-rays
    F_D_DIST = 'sample and detector distance',
    PIXEL_SIZE = 'pixel size'
    TWO_THETA = 'two_theta',
    ALPHA_I = 'incident angle',
    ALPHA_F = 'scattered angle',
    OMEGA = 'omega',
    DETECTOR_SIZE = 'size of the detector (H, W)'
    NUM_SCANS = 'number of scans along px and py',
    NUM_STEPS = 'number of steps'
    STEP_SIZE = 'step size along px and py'

pars = {PARAMETERS_NAMES.E: 12, PARAMETERS_NAMES.F_D_DIST: 1.2, PARAMETERS_NAMES.PIXEL_SIZE: 2*55e-6,
        PARAMETERS_NAMES.TWO_THETA: 15.6 * 2 * np.pi / 180, PARAMETERS_NAMES.ALPHA_I: 15.6 * np.pi / 180,
        PARAMETERS_NAMES.OMEGA: 0.016*np.pi/180, PARAMETERS_NAMES.DETECTOR_SIZE: (200, 180),
        PARAMETERS_NAMES.NUM_STEPS: 27, PARAMETERS_NAMES.NUM_SCANS: (41, 45), PARAMETERS_NAMES.STEP_SIZE: (50e-9, 50e-9)}

pars[PARAMETERS_NAMES.LAMBDA] = 1.239e-9 / pars[PARAMETERS_NAMES.E]
pars[PARAMETERS_NAMES.ALPHA_F] = pars[PARAMETERS_NAMES.OMEGA] - pars[PARAMETERS_NAMES.ALPHA_I]
