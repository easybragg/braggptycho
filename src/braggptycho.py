import numpy as np


class BraggPtychoData:

    def __init__(self,
        iobs: np.ndarray,
        wavelength: float,
        detector_distance: float,
        pixel_size_detector: float,
        twotheta_angle: float,
        positions: np.ndarray,
        angles: np.ndarray,
        is_3d: bool = True,
        angular_oversampling: bool = False):

        """
        Class for Bragg ptychography data.


        :param iobs: 4d numpy array of shape (nb_pos, nb_angle, ny, nx)
        :param wavelength: wavelength of the experiment in meters
        :param detector_distance: sample-detector distance in meters
        :param pixel_size_detector: size of the detector's pixel (square pixels)
        :param twotheta_angle: value of the two theta angle, in radians ?
        :param positions: 2d numpy array of the positions already converted to the final reconstructed frame.
                          For 3D BP, only a (nb_pos, 2) array for posx and posy
                          For general BP, the positions are independent for each frame, so an array of shape
                          (nb_frame, 2) is needed for posx and posy
        :param angles: array with the rocking curve angle values
        :param is_3d: True if 3D BP
        :param angular_oversampling: True if angular oversampling is needed
        """

        self._wavelength = wavelength
        self._detector_distance = detector_distance
        self._pixel_size_detector = pixel_size_detector
        self._twotheta_angle = twotheta_angle
        self._positions = positions
        self._angles = angles
        self._is_3d = is_3d
        self._angular_oversampling = angular_oversampling

        # TODO: to check
        self._alpha_i = self._alpha_f = 0.5 * self._twotheta_angle
        self._domega = (self._angles[-1] - self.angles[0]) / len(self._angles)

    def pixel_size_q(self):
        """
        Get the q1, q2, q3 pixel size in detector space and project the rocking curve direction vector into the scattering vector
        to obtain an orthogonal frame
        """

        pixel_size_q1 = self._wavelength * self._pixel_size_detector / self._detector_distance
        pixel_size_q3 = self._wavelength * (np.sin(self._alpha_i) + np.sin(self._alpha_f)) * np.sin(self._domega) * np.cos(self._alpha_f)

        return pixel_size_q1, pixel_size_q1, pixel_size_q3

    def pixel_size_recon(self):
        """
        Get the r1, r2 and r3 pixel size in the reconstruction frame.
        """

        dq1, dq2, dq3 = self.pixel_size_q()

        # number of vertical and horizontal detector pixels
        ny, nx = self.iobs.shape[-2:]
        # number of angular steps
        nang = len(self._angles)

        # From the FFT conjugation and assuming q1, q2, q3 orthogonal
        pixel_size_r1 = 2 * np.pi / (nx * dq1)
        pixel_size_r2 = 2 * np.pi / (ny * dq2)
        pixel_size_r3 = 2 * np.pi / (nang * dq3)

        return pixel_size_r1, pixel_size_r2, pixel_size_r3

    def pixel_size_object(self):
        """
        Get the x, y and z pixel size in the object reference frame.
        """

        dr1, dr2, dr3 = pixel_size_recon()

        pixel_size_x = dr1 / np.cos(self._alpha_f)
        pixel_size_y = dr2
        pixel_size_z = dr3 * np.cos(self._alpha_f)

        return pixel_size_x, pixel_size_y, pixel_size_z

    def pixel_size_illumination(self):
        """
        Get the xi, yi and zi pixel size in the illumination reference frame.
        """

        dx, dy, dz = pixel_size_object()

        pixel_size_xi = dx / np.cos(self._alpha_i)
        pixel_size_yi = dy
        pixel_size_zi = dx * np.cos(self._alpha_i)

        return pixel_size_xi, pixel_size_yi, pixel_size_zi


class BraggPtychoLoader:

    def __init__(self, data_path):

        pass

    def read_scan(self):
        pass

    def read_data(self):
        pass

    def data_post_process(self):
        pass

    def prepare_geomtry(self):
        pass

    def load_probe(self):
        pass


class BraggPtycho:

    def __init__(self):
        pass

