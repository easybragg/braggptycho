from parameters import PARAMETERS_NAMES as P
import numpy as np


class Sampling:

    def __init__(self, pars):

        self._pars = pars

        # Obtain pixel size in the measurement frame q1, q2, q3
        # Assuming that the angle delta between a pixel of the detecor is very small, we have tan(delta)=delta, sin(delta)=delta
        # The pixel size projection onto the q space is then the modulus of the scattering vector times pixel_size/f_d_dist

        pxl_dst = pars[P.PIXEL_SIZE] / pars[P.F_D_DIST]

        # q = (q1, q2, q3)
        self._q = 2 * np.pi * pars[P.LAMBDA] * np.array([pxl_dst, pxl_dst, (np.sin(pars[P.ALPHA_I]) + np.sin(pars[P.ALPHA_F])) * 
            np.sin(pars[P.OMEGA]) * np.cos(pars[P.ALPHA_F])])

        # Compute the biggest shift performed along q1 TODO
        # self._q1_shift = np.round(self._q[2] * p3 * np.tan(ALPHA_F) / self._q[0])

        # Pixel size in the measurement conjugate frame r = (r1, r2, r3)
        self._r = 2 * np.pi / self._q * np.array([H, W, step_num])

        # Pixel size in the object frame = x, y, z
        self._frame = np.array([self._r[0] / np.cos(pars[P.ALPHA_F]), self._r[1], self._r[2] * np.cos(pars[P.ALPHA_F])])

        # Pixel size in the illumination frame iframe = (xi, yi, zi)
        self._iframe = np.array([self._frame[0] / np.cos(pars[P.ALPHA_I]), self._frame[1], self._frame[2] * np.cos(pars[P.ALPHA_I])])


class Experiment:

    def __init__(self, pars : dict):
        self._pars = pars

        self._sampling = Sampling(pars)

        self.generate_coordinates()

    def generate_coordinates(self):
        self.coord = np.meshgrid(np.arange(self._pars[P.NUM_SCANS][0]) * self._pars[P.STEP_SIZE][0],
                                 np.arange(self._pars[P.NUM_SCANS][1]) * self._pars[P.STEP_SIZE][1])

    def load_mask(self, path):
        self._mask = np.load(path)

    def init_probe_guess(self, path):
        pass

    def swap_axes(self):
        self.coord = self.coord[1], self.coord[0]

    def inverse_sign(axis):
        self.coord[axis] = -self.coord[axis]

    def obtain_r_coordinates():

        self._r_coordinates = np.array([self.coord[0] * np.sin(self._pars[P.ALPHA_F]),
                                        -self.coord[1],
                                        -self.coord[0] * np.cos(self._pars[P.ALPHA_F])])
